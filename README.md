# Apache 2.4 Connector for Crowd 
 
Based on [Eugene Yunak](https://bitbucket.org/atlassian/cwdapache/pull-request/21/support-for-apache-httpd-24/diff) and [mathias_burger](https://bitbucket.org/atlassian/cwdapache/pull-request/18/added-apache-24-compatibility-and-fixed/diff) patches

# PPA

     sudo add-apt-repository ppa:adamansky/crowd-apache2-modules
     sudo apt-get update 
     sudo apt-get install libapache2-mod-authnz-crowd
     sudo service apache2 reload

# Apache config example

```
 <Directory "/some/resource">
          AuthName "Foo"
          AuthType Basic
          AuthBasicProvider crowd
          CrowdAppName apache
          CrowdAppPassword password
          CrowdURL http://localhost:8095/crowd/
          
          Require valid-user  
          #Require crowd-group groupname 
  </Directory>
```